package people

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/jinzhu/copier"
	"gitlab.com/golang-studies/api-first/internal/pkg/people/response"
)

//
func (p *People) CreatePerson(r *http.Request) (*response.ResponsePerson, error) {

	prs := p.NewPerson()

	err := json.NewDecoder(r.Body).Decode(&prs)
	if err != nil {
		return nil, err
	}

	idPerson := strconv.Itoa(len(p.people) + 1)

	p.people[idPerson] = *prs
	rspPerson := new(response.ResponsePerson)

	copier.Copy(rspPerson, prs)

	rspPerson.Idx, _ = strconv.Atoi(idPerson)
	return rspPerson, nil
}
