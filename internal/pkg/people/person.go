package people

type Person struct {
	Name    string `json: name`
	Age     int    `json: age`
	Address string `json: address`
	Job     string `json: job`
}

type People struct {
	people map[string]Person
}

func NewPeople() *People {
	return &People{
		people: make(map[string]Person),
	}
}

func (p *People) NewPerson() *Person {
	return &Person{
		Name:    "",
		Age:     0,
		Address: "",
		Job:     "",
	}
}
