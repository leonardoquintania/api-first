package response

type ResponsePerson struct {
	Idx     int    `json: id`
	Name    string `json: name`
	Age     int    `json: age`
	Address string `json: address`
	Job     string `json: job`
}
