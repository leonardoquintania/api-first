package people

import (
	"errors"
	"strconv"

	"github.com/jinzhu/copier"
	"gitlab.com/golang-studies/api-first/internal/pkg/people/response"
)

func (p *People) GetPersonId(idPerson int) (*response.ResponsePerson, error) {

	if len(p.people) < idPerson {
		return nil, errors.New("invalid id! ")
	}

	id := strconv.Itoa(idPerson)

	println(id)
	prs := p.people[id]

	rspPerson := new(response.ResponsePerson)

	copier.Copy(rspPerson, prs)

	rspPerson.Idx = idPerson
	return rspPerson, nil
}
