package main

import (
	"log"
	"net/http"

	"gitlab.com/golang-studies/api-first/cmd/server/router"
)

func main() {

	r := router.NewRouter()
	r.RegisterRoutesGET("/search")
	r.RegisterRoutesPOST("/admin")
	println("..... Start Server ......")
	println("..Port :8081")
	println(".........................")

	log.Fatal(http.ListenAndServe(":8081", r.App))
}
