package router

import (
	"net/http"

	"gitlab.com/golang-studies/api-first/cmd/server/router/handlers"
)

type Router struct {
	App     *http.ServeMux
	Handler *handlers.Handlers
}

func NewRouter() *Router {
	return &Router{
		App:     http.NewServeMux(),
		Handler: handlers.NewHandlers(),
	}
}

func (r *Router) RegisterRoutesPOST(endpoit string) {
	r.App.HandleFunc(endpoit, r.Handler.CreatePerson)
}

func (r *Router) RegisterRoutesGET(endpoit string) {
	r.App.HandleFunc(endpoit, r.Handler.GetPersonId)
}
